import 'package:flutter/material.dart';
import 'package:shopping_list/screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shopping List',
      theme: ThemeData.light().copyWith(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Color.fromARGB(255, 25, 65, 43), // Il colore seme
          surface: Color.fromARGB(255, 255, 205, 171),
          primary: Color.fromARGB(255, 241, 129, 54),
          secondary: Color.fromARGB(255, 243, 243, 243),
          brightness: Brightness.light,
        ),
        scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255),
        primaryColor: Color.fromARGB(255, 243, 243, 243),
      ),
      home: const HomeScreen(),
    );
  }
}
