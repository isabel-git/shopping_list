import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopping_list/data/categories.dart';
import 'package:shopping_list/models/category.dart';
import 'package:http/http.dart' as http;
import 'package:shopping_list/models/shopping_item.dart'; //as è una keyword che indica a dart di ragruppare tutti i contenuti forniti dal pacchetto nell'oggetto che ho chiamato http

class NewItemScreen extends StatefulWidget {
  const NewItemScreen({super.key});

  @override
  State<NewItemScreen> createState() => _NewItemScreenState();
}

class _NewItemScreenState extends State<NewItemScreen> {
  // we use a global key to access the form
  final _formKey = GlobalKey<FormState>();
  var _enteredName = '';
  var _enteredQuantity = 1;
  var _selctedCategory = categories[Categories.vegetables]!;

  bool _saveItem = false;

  void _seveItem() async {
    //esegue in automatico le funzioni di validazione nei campi di testo e restituisce un valore booleano
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save(); //esegue in automatico la funzione onSaved

      setState(() {
        _saveItem = true;
      });

      // Uri.https otteniamo l'indirizzo a cui inviare la richiesta
      final url = Uri.https(
          'flutter-test-ba554-default-rtdb.europe-west1.firebasedatabase.app',
          'shopping-list.json'); //shopping-list è il percorso, mentre .json è richiesto da Firebase

      // utilizziamo l'oggetto http (creato con l'aiuto di as) per inviare una richiesta a Firebase
      final response = await http.post(
        url,
        //headers: aiutano firebase a capire come saranno formattati i dati che gli invieremo
        headers: {
          'Content-Type': 'application/json',
        },
        // i body: indicano i dati da allegare alla richiesta in uscita (ovvera la spesa)
        body:
            // Json.encode converte una mappa in json
            json.encode(
          {
            // non è necessario inviare l'id
            'name': _enteredName,
            'quantity': _enteredQuantity,
            'category': _selctedCategory.label, //memorizzo solo il testo
          },
        ),
      );

      //memorizziamo i dati della risposta
      final Map<String, dynamic> resData = json.decode(response.body);

      //context.mounted restituisce true se il widget e' stato montat (se il widget fa parte ancora dello schermo)
      //di seguito verifichiamo se non e' stato ancora montato (se il widget non fa piu' parte dello schermo)
      if (!context.mounted) {
        return;
      }

      // passiamo i dati al metodo pop
      Navigator.of(context).pop(
        ShoppingItem(
          id: resData['name'],
          name: _enteredName,
          quantity: _enteredQuantity,
          category: _selctedCategory,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.scaffoldBackgroundColor,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15.0, right: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'New product',
              style: theme.textTheme.titleLarge!.copyWith(fontSize: 30),
            ),
            const SizedBox(
              height: 10,
            ),
            _saveItem
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Form(
                    key: _formKey,
                    child: Expanded(
                      child: Column(
                        children: [
                          TextFormField(
                            maxLength: 50,
                            decoration:
                                const InputDecoration(label: Text('Name')),
                            validator: (value) {
                              //value.trim() remove empty spaces
                              //the value is not valid if length value is equal between 1 and 50
                              if (value == null ||
                                  value.isEmpty ||
                                  value.trim().length == 1 ||
                                  value.trim().length > 50) {
                                return 'Must be between 1 and 50 characters!';
                              }
                              //it's valid if value is null
                              return null;
                            },
                            //value e' il valore al momento dell'esecuzione durante il salvtaggio
                            onSaved: (value) {
                              _enteredName = value!;
                            },
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              // Add Expanded to ensure proper layout constraints
                              Expanded(
                                child: TextFormField(
                                  decoration: const InputDecoration(
                                      label: Text('Quantity')),
                                  keyboardType: TextInputType.number,
                                  initialValue: _enteredQuantity.toString(),
                                  validator: (value) {
                                    //int.tryParse(value) converts the value in number and returns null if fails to convert it
                                    //the value is not valid if value isn't a number or it's less then 0
                                    if (value == null ||
                                        value.isEmpty ||
                                        int.tryParse(value) == null ||
                                        int.tryParse(value)! <= 0) {
                                      return 'Must be a valid, positive number!';
                                    }
                                    //it's valid if value is null
                                    return null;
                                  },
                                  //value e' il valore al momento dell'esecuzione durante il salvtaggio
                                  onSaved: (value) {
                                    _enteredQuantity = int.parse(value!);
                                  },
                                ),
                              ),
                              const SizedBox(width: 8),
                              // Add Expanded to ensure proper layout constraints
                              Expanded(
                                child: DropdownButtonFormField(
                                  dropdownColor: theme.colorScheme.surface,
                                  value: _selctedCategory,
                                  items: [
                                    //entries proprety: converts a map to a list
                                    for (final category in categories.entries)
                                      DropdownMenuItem(
                                        value: category.value,
                                        child: Row(
                                          children: [
                                            Icon(category.value.icon),
                                            const SizedBox(width: 6),
                                            Text(category.value.label)
                                          ],
                                        ),
                                      )
                                  ],
                                  onChanged: (value) {
                                    //si aggiorna visivamente ogni volta che viene effettuata una nuova selezione
                                    setState(() {
                                      _selctedCategory = value!;
                                    });
                                  },
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 18),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                onPressed: _saveItem
                                    ? null
                                    : () {
                                        _formKey.currentState!.reset();
                                      },
                                child: const Text('Reset'),
                              ),
                              const SizedBox(width: 8),
                              ElevatedButton(
                                onPressed: _saveItem ? null : _seveItem,
                                child: const Text('Add'),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
