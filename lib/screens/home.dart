import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopping_list/data/categories.dart';
import 'package:shopping_list/screens/new_item.dart';
import 'package:shopping_list/widgets/bottom_navigation.dart';
import 'package:shopping_list/widgets/shopping_item.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<ShoppingItem> _shoppingItems = [];
  bool _isLoading = true;
  String? _error;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _loadItems();
  }

  void _loadItems() async {
    final url = Uri.https(
        'flutter-test-ba554-default-rtdb.europe-west1.firebasedatabase.app',
        'shopping-list.json');

    try {
      final response = await http.get(url);

      if (response.statusCode >= 400) {
        setState(() {
          _error = 'Failed to fetch data. Please try again later';
        });
      }

      if (response.body == 'null') {
        setState(() {
          _isLoading = false;
        });
        return;
      }

      // Json.decode converte i dati JSON in MAP
      final Map<String, dynamic> listData = json.decode(response.body);
      final List<ShoppingItem> loadedItems = [];

      //entries è un elenco di elementi in cui ogni elemento ha una proprietà key e una value
      for (var item in listData.entries) {
        // firstWhere confronta il titolo della categoria con i dati della categoria in memoria e restituisce il primo elemento che soddisfa la condizione
        final category = categories.entries
            .firstWhere(
                (element) => element.value.label == item.value['category'])
            .value;
        loadedItems.add(
          ShoppingItem(
            id: item.key,
            name: item.value['name'],
            quantity: item.value['quantity'],
            // category: item.value[category'], // NON va bene, in questo modo non passo l'intera categoria, ma solo il titolo
            category: category,
          ),
        );
      }
      setState(() {
        _shoppingItems = loadedItems;
        _isLoading = false;
      });
    } catch (e) {
      _error = 'Failed to fetch shopping items. Please try later.';
    }
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<ShoppingItem>(
        MaterialPageRoute(builder: (ctx) => const NewItemScreen()));

    // verifica se ci sono nuovi elementi
    if (newItem == null) {
      return;
    }
    setState(() {
      _shoppingItems.add(newItem);
    });
  }

  void _removeItem(ShoppingItem item) async {
    final index = _shoppingItems.indexOf(item);
    // Rimuovi l'elemento dalla lista
    setState(() {
      _shoppingItems.removeAt(index);
    });

    final url = Uri.https(
        'flutter-test-ba554-default-rtdb.europe-west1.firebasedatabase.app',
        'shopping-list/${item.id}.json');

    final response = await http.delete(url);

    if (response.statusCode >= 400) {
      if (!context.mounted) {
        return;
      }

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Error deleting item. Try again.'),
        ),
      );
      setState(() {
        //insert è un metodo di dart usato sugli elenchi per aggiungere un elemento ad un indice specifico
        // In caso di errore ripristina l'elemento nella lista
        _shoppingItems.insert(index, item);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    Widget content = Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 250,
            child:
                Image.asset('assets/images/shopping-basket-with-groceries.png'),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            'No items added yet.',
            style: theme.textTheme.titleMedium,
          ),
        ],
      ),
    );

    if (_isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_shoppingItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _shoppingItems.length,
        itemBuilder: (ctx, index) => Dismissible(
          onDismissed: (direction) {
            _removeItem(_shoppingItems[index]);
          },
          key: ValueKey(_shoppingItems[index].id),
          child: ShoppingItemWidget(
              icon: _shoppingItems[index].category.icon,
              label: _shoppingItems[index].name,
              quantity: _shoppingItems[index].quantity),
        ),
      );
    }

    if (_error != null) {
      content = Center(
        child: Text(_error!),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.scaffoldBackgroundColor,
      ),
      bottomNavigationBar: BottomNavigation(
        addItem: _addItem,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15.0, right: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'My shopping',
              style: theme.textTheme.titleLarge!.copyWith(fontSize: 30),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(child: content),
          ],
        ),
      ),
    );
  }
}
