import 'package:flutter/material.dart';

class ShoppingItemWidget extends StatelessWidget {
  const ShoppingItemWidget(
      {super.key,
      required this.icon,
      required this.label,
      required this.quantity});

  final IconData icon;
  final String label;
  final int quantity;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(top: 3, bottom: 3),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: theme.primaryColor,
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            children: [
              Icon(
                icon,
                size: 30,
              ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(label, style: theme.textTheme.bodyLarge!),
                  Text(
                    quantity.toString(),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
