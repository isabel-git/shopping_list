import 'package:flutter/material.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({super.key, required this.addItem});

  final void Function() addItem;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return BottomAppBar(
      color: theme.scaffoldBackgroundColor,
      child: Center(
        child: Container(
          width: 56.0, // Larghezza predefinita di IconButton
          height: 56.0, // Altezza predefinita di IconButton
          decoration: BoxDecoration(
            shape: BoxShape.rectangle, // Modifica della forma del Container
            borderRadius: BorderRadius.circular(10.0), // Bordo arrotondato
            color: theme.colorScheme.primary, // Colore di sfondo dell'icona
          ),
          child: IconButton(
            icon: const Icon(Icons.add),
            onPressed: addItem,
            tooltip: 'Add new item',
            color: Colors.white, // Personalizza il colore dell'icona
            iconSize: 30.0, // Personalizza la dimensione dell'icona
            splashRadius: 20.0, // Personalizza il raggio dell'effetto splash
          ),
        ),
      ),
    );
  }
}
