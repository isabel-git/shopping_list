import 'package:flutter/material.dart';

import 'package:shopping_list/models/category.dart';

const categories = {
  Categories.vegetables: Category(
    'Vegetables',
    Icons.eco,
  ),
  Categories.fruit: Category(
    'Fruit',
    Icons.apple,
  ),
  Categories.meat: Category(
    'Meat',
    Icons.set_meal_rounded,
  ),
  Categories.dairy: Category(
    'Dairy',
    Icons.egg,
  ),
  Categories.carbs: Category(
    'Carbs',
    Icons.local_pizza_sharp,
  ),
  Categories.sweets: Category(
    'Sweets',
    Icons.icecream_outlined,
  ),
  Categories.spices: Category(
    'Spices',
    Icons.grain,
  ),
  Categories.convenience: Category(
    'Convenience',
    Icons.fastfood_rounded,
  ),
  Categories.hygiene: Category(
    'Hygiene',
    Icons.medication_liquid_rounded,
  ),
  Categories.other: Category(
    'Other',
    Icons.restaurant,
  ),
};
