# Shopping List

A Flutter-based shopping list application to manage your grocery items efficiently. 

## Features

- **Add Items:** Easily add new items to your shopping list.
- **Delete Items:** Remove items from the list with a swipe.
- **Firebase Integration:** Store and retrieve items from Firebase.
- **Error Handling:** Displays error messages for network issues.


## Screenshots

### Home Screen and New Items Screen
<div align="center">
  <img src="screenshots/home_1.png" alt="Home Screen" width="200" />
  <img src="screenshots/add_item.png" alt="New Items Screen" width="200" />
</div>

### Loading and Grocery list
<div align="center">
  <img src="screenshots/home_2.png" alt="Loading" width="200" />
  <img src="screenshots/home_3.png" alt="Grocery List" width="200" />
</div>

## Installation

1. **Clone the repository:**
   ```sh
   git clone https://gitlab.com/isabel-git/shopping_list.git

2. **Navigate to the project directory:**
    ```sh
   cd shopping_list

3. **Install dependencies:**
   ```sh
   flutter pub get

4. **Run the app:**
   ```sh
   flutter run
